Attribute VB_Name = "DailyTrackerModule"
Option Explicit

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "MX1-London Operations Department"
Const g_strFullUSerName = "MX1-London Operations"

Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Public g_strConnection As String

Sub Main()

Dim cnn As ADODB.Connection, rst As ADODB.Recordset, rstclip As ADODB.Recordset, rstHoliday As ADODB.Recordset
Dim SQL As String, DaysPast As Integer, l_strCommandLine As String
Dim emailtext As String, ClipID As Long, TempDate As String, Channel As String
Dim EmailTo As String, EmailName As String
Dim CountDown As Long
Dim NowDate As Date

Set cnn = New ADODB.Connection

Dim CommandString As String
CommandString = Command()

g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bssql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

'g_strConnection = "DRIVER=SQL Server;SERVER=jca-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=MX1TEST-sql-3;Failover_Partner=MX1TEST-sql-4;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

cnn.Open g_strConnection

Set rst = New ADODB.Recordset
'Set rstclip = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strUserEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strUserEmailName = rst("value")
End If
rst.Close

'Run through BBCWW, looking for tapes that have either arrived, or not arrived that cannot meet the deadline under the SLA
emailtext = ""
Set rst = cnn.Execute("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & 3)
NowDate = rst(0)
rst.Close

SQL = "SELECT * FROM vw_Tracker_BBCWW WHERE completeddate IS NULL AND cancelleddate IS NULL AND rejecteddate IS NULL AND companyID = 570 AND MasterBarcode IS NOT NULL and MasterBarcode <> '' AND jobstatus <> 'Sent To Accounts' AND jobstatus <> 'Invoiced' AND jobstatus <> 'No Charge' AND jobstatus <> 'Cancelled' AND ((Targetdate IS NOT NULL AND CAST(Targetdate AS DATE) > cast(deadlinedate AS DATE)) OR (TargetDate IS NULL AND deadlinedate < '" & Format(NowDate, "YYYY-MM-DD") & "')) ORDER BY TargetDate;"
rst.Open SQL, cnn, adOpenStatic, adLockOptimistic

If rst.RecordCount <> 0 Then

    emailtext = emailtext & "The following items cannot be completed by the original Deadline Date, because masters either have not arrived at RR media yet, or they arrived too late:" & vbCrLf & vbCrLf
    rst.MoveFirst
    Do While Not rst.EOF
        emailtext = emailtext & "Barcode: " & rst("MasterBarcode") & ", JobID: " & rst("jobID") & ", Star Order Number: " & rst("orderreference") & ", Original Deadline: " & Format(rst("deadlinedate"), "DD/MM/YYYY hh:nn:ss") & ", Arrived: " & IIf(Not IsNull(rst("DateMasterArrived")), Format(rst("DateMasterArrived"), "DD/MM/YYYY hh:nn:ss"), "Not arrived") & IIf(Not IsNull(rst("Targetdate")), ", Target: " & Format(rst("TargetDate"), "DD/MM/YYYY hh:nn:ss"), "") & vbCrLf
        rst.MoveNext
    Loop
End If

rst.Close

Debug.Print emailtext

If emailtext <> "" Then

    emailtext = emailtext & vbCrLf & vbCrLf & "Please contact the RR team to discuss any of the above."
    SQL = "SELECT * FROM trackernotification WHERE contractgroup = 'BBC' AND trackermessageID = 55;"
    rst.Open SQL, cnn, 3, 3
    If rst.RecordCount > 0 Then
        rst.MoveFirst
        Do While Not rst.EOF
            SendSMTPMail rst("email"), rst("fullname"), "BBCWW Int Ops Tape->Tape Job(s) that cannot meet their Deadline Date", emailtext, "", True, "", ""
            rst.MoveNext
        Loop
    End If
    rst.Close
    SQL = "INSERT INTO Dashboard_Messages (Contact_Group, Tracker_Message_ID, Message_Title, Message_Body) VALUES ('BBC', 55, 'BBCWW Int Ops Tape->Tape Job(s) that cannot meet their Deadline Date', '" & emailtext & "');"
    cnn.Execute SQL

End If

'Second, run through the platform tracker listing uncompleted items in their Time brackets
emailtext = ""
NowDate = Now
SQL = "SELECT * FROM tracker_itunes_item WHERE readytobill = 0 AND (rejected IS NULL or rejected = 0) AND (assetshere <> 0) AND CompanyID IN (1163, 1489) AND requiredby <= '" & FormatSQLDate(DateAdd("h", 24, NowDate)) & "' ORDER BY requiredby;"
rst.Open SQL, cnn, adOpenStatic, adLockOptimistic

If rst.RecordCount <> 0 Then

    emailtext = emailtext & "The following workable items are either overdue, or due to be done within 24 hours:" & vbCrLf & vbCrLf
    rst.MoveFirst
    Do While Not rst.EOF
        emailtext = emailtext & "Order Type: " & rst("iTunesOrderType") & ", Title: " & rst("Title") & ", Title: " & rst("Title") & ", Episode: " & rst("Episode") & ", Due Date: " & rst("requiredby") & vbCrLf
        rst.MoveNext
    Loop
End If

rst.Close

SQL = "SELECT * FROM tracker_itunes_item WHERE readytobill = 0 AND (rejected IS NULL or rejected = 0) AND (assetshere <> 0) AND CompanyID IN (1163, 1489) AND requiredby > '" & FormatSQLDate(DateAdd("h", 24, NowDate)) & "' AND requiredby <= '" & FormatSQLDate(DateAdd("h", 48, NowDate)) & "' ORDER BY requiredby;"
rst.Open SQL, cnn, adOpenStatic, adLockOptimistic

If rst.RecordCount <> 0 Then

    emailtext = emailtext & vbCrLf & "The following workable items are due to be done within 2 days:" & vbCrLf & vbCrLf
    rst.MoveFirst
    Do While Not rst.EOF
        emailtext = emailtext & "Order Type: " & rst("iTunesOrderType") & ", Title: " & rst("Title") & ", Title: " & rst("Title") & ", Episode: " & rst("Episode") & ", Due Date: " & rst("requiredby") & vbCrLf
        rst.MoveNext
    Loop
End If

rst.Close

SQL = "SELECT * FROM tracker_itunes_item WHERE readytobill = 0 AND (rejected IS NULL or rejected = 0) AND (assetshere <> 0) AND CompanyID IN (1163, 1489) AND requiredby > '" & FormatSQLDate(DateAdd("h", 48, NowDate)) & "' AND requiredby <= '" & FormatSQLDate(DateAdd("h", 96, NowDate)) & "' ORDER BY requiredby;"
rst.Open SQL, cnn, adOpenStatic, adLockOptimistic

If rst.RecordCount <> 0 Then

    emailtext = emailtext & vbCrLf & "The following workable items are due to be done within 4 days:" & vbCrLf & vbCrLf
    rst.MoveFirst
    Do While Not rst.EOF
        emailtext = emailtext & "Order Type: " & rst("iTunesOrderType") & ", Title: " & rst("Title") & ", Title: " & rst("Title") & ", Episode: " & rst("Episode") & ", Due Date: " & rst("requiredby") & vbCrLf
        rst.MoveNext
    Loop
End If

rst.Close

SQL = "SELECT * FROM tracker_itunes_item WHERE readytobill = 0 AND (rejected IS NULL or rejected = 0) AND (assetshere <> 0) AND CompanyID IN (1163, 1489) AND requiredby > '" & FormatSQLDate(DateAdd("h", 96, NowDate)) & "' ORDER BY requiredby;"
rst.Open SQL, cnn, adOpenStatic, adLockOptimistic

If rst.RecordCount <> 0 Then

    emailtext = emailtext & vbCrLf & "The following workable items are due to be done more than 4 days from now:" & vbCrLf & vbCrLf
    rst.MoveFirst
    Do While Not rst.EOF
        emailtext = emailtext & "Order Type: " & rst("iTunesOrderType") & ", Title: " & rst("Title") & ", Title: " & rst("Title") & ", Episode: " & rst("Episode") & ", Due Date: " & rst("requiredby") & vbCrLf
        rst.MoveNext
    Loop
End If

rst.Close

Debug.Print emailtext

If emailtext <> "" Then

    SQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DISNEY' AND trackermessageID = 36;"
    rst.Open SQL, cnn, 3, 3
    If rst.RecordCount > 0 Then
        rst.MoveFirst
        Do While Not rst.EOF
            SendSMTPMail rst("email"), rst("fullname"), "Platform Tracker work that can (and should) be done", emailtext, "", True, "", ""
            rst.MoveNext
        Loop
    End If
    rst.Close
    SQL = "INSERT INTO Dashboard_Messages (Contact_Group, Tracker_Message_ID, Message_Title, Message_Body) VALUES ('DISNEY', 36, 'Platform Tracker work that can (and should) be done', '" & QuoteSanitise(emailtext) & "');"
    cnn.Execute SQL

End If

cnn.Close

'Set rstclip = Nothing
Set rst = Nothing
Set cnn = Nothing

End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
100 l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

102 l_con.Open "dsn=cetasql"
104 l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

106 If Not l_rstGetData.EOF Then
    
108     If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
110         GetData = l_rstGetData(lp_strFieldToReturn)
112         GoTo Proc_CloseDB
        End If

    Else
114     GoTo Proc_CloseDB

    End If

116 Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
118     GetData = ""
120 Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
122     GetData = 0
124 Case adDate, adDBDate, adDBTime, adDBTimeStamp
126     GetData = 0
128 Case Else
130     GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

132 l_rstGetData.Close
134 Set l_rstGetData = Nothing

136 l_con.Close
138 Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Format(lDate, "YYYY-MM-DD HH:NN:SS")
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function QuoteSanitise(ByVal lp_strText As Variant) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strNewText As String
    Dim l_strOldText As String
    Dim l_intLoop As Long
    l_strNewText = ""
    
    If IsNull(lp_strText) Then
        l_strOldText = ""
        l_strNewText = l_strOldText
        lp_strText = ""
    Else
        l_strOldText = lp_strText
    End If
    
    If InStr(lp_strText, "'") <> 0 Or InStr(lp_strText, """") <> 0 Then
        
        For l_intLoop = 1 To Len(l_strOldText)
            
            Select Case Mid(l_strOldText, l_intLoop, 1)
                
                Case "'"
                    l_strNewText = l_strNewText & "''"
'                Case Chr(34)
'                    l_strNewText = l_strNewText & Chr(34) & Chr(34)
                Case Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
            
            End Select
            
        Next
    Else
        l_strNewText = lp_strText
    End If
    
    If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
        l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
    End If
    
    QuoteSanitise = l_strNewText
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function


