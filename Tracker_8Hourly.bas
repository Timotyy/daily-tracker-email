Attribute VB_Name = "Tracker_8Hourly"
Option Explicit

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "MX1 London Operations Department"
Const g_strFullUSerName = "MX1 London Operations"

Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Public cnn As ADODB.Connection
Public g_strConnection As String

Sub Main()

Dim rst As ADODB.Recordset, rstclip As ADODB.Recordset
Dim SQL As String, DaysPast As Integer, l_strCommandLine As String
Dim emailtext As String, ClipID As Long, TempDate As String, Channel As String
Dim EmailTo As String, EmailName As String
Dim NowDate As Date

Dim CommandString As String
CommandString = Command()

g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bssql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

Set cnn = New ADODB.Connection

SQL = g_strConnection
'SQL = "DRIVER=SQL Server;SERVER=jca-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'SQL = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=MX1TEST-sql-3;Failover_Partner=MX1TEST-sql-4;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

cnn.Open SQL

Set rst = New ADODB.Recordset
'Set rstclip = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strUserEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strUserEmailName = rst("value")
End If
rst.Close

NowDate = Now

''First run through BBCMG for items that are failed SLA or within 4 hours of SLA
'emailtext = ""
'SQL = "SELECT * FROM BBCMG_Preview_Tracker WHERE DatePreviewReady IS NULL and Targetdate <= '" & Format(DateAdd("h", 4, NowDate), "YYYY-MM-DD hh:nn:ss") & "' ORDER BY TargetDate;"
'rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
'
'If rst.RecordCount <> 0 Then
'
'    emailtext = emailtext & "The following items have either failed SLA or will fail if not completed in 4 hours:" & vbCrLf & vbCrLf
'    rst.MoveFirst
'    Do While Not rst.EOF
'        If GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Tape Arrived") _
'        Or GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Ingest in Process") _
'        Or GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Preview Generation in Process") Then
'            emailtext = emailtext & "Barcode: " & rst("Barcode") & ", Arrived: " & rst("DateTapeArrived") & ", Target: " & rst("TargetDate") & ", Status: " & GetData("BBCMG_Order_Status", "Order_Status", "Order_Status_ID", GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID"))) & vbCrLf
'        End If
'        rst.MoveNext
'    Loop
'End If
'
'rst.Close
'
'SQL = "SELECT * FROM BBCMG_Preview_Tracker WHERE DatePreviewReady IS NULL and Targetdate <= '" & Format(DateAdd("h", 8, NowDate), "YYYY-MM-DD hh:nn:ss") & "' AND Targetdate > '" & Format(DateAdd("h", 4, NowDate), "YYYY-MM-DD hh:nn:ss") & "' ORDER BY TargetDate;"
'rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
'
'If rst.RecordCount <> 0 Then
'
'    emailtext = emailtext & vbCrLf & "The following items are between 8 hours and 4 hours to go:" & vbCrLf & vbCrLf
'    rst.MoveFirst
'    Do While Not rst.EOF
'        If GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Tape Arrived") _
'        Or GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Ingest in Process") _
'        Or GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Preview Generation in Process") Then
'            emailtext = emailtext & "Barcode: " & rst("Barcode") & ", Arrived: " & rst("DateTapeArrived") & ", Target: " & rst("TargetDate") & ", Status: " & GetData("BBCMG_Order_Status", "Order_Status", "Order_Status_ID", GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID"))) & vbCrLf
'        End If
'        rst.MoveNext
'    Loop
'End If
'
'rst.Close
'
'SQL = "SELECT * FROM BBCMG_Preview_Tracker WHERE DatePreviewReady IS NULL and Targetdate <= '" & Format(DateAdd("h", 24, NowDate), "YYYY-MM-DD hh:nn:ss") & "' AND Targetdate > '" & Format(DateAdd("h", 8, NowDate), "YYYY-MM-DD hh:nn:ss") & "' ORDER BY TargetDate;"
'rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
'
'If rst.RecordCount <> 0 Then
'
'    emailtext = emailtext & vbCrLf & "The following items are between 24 hours and 8 hours to go:" & vbCrLf & vbCrLf
'    rst.MoveFirst
'    Do While Not rst.EOF
'        If GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Tape Arrived") _
'        Or GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Ingest in Process") _
'        Or GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Preview Generation in Process") Then
'            emailtext = emailtext & "Barcode: " & rst("Barcode") & ", Arrived: " & rst("DateTapeArrived") & ", Target: " & rst("TargetDate") & ", Status: " & GetData("BBCMG_Order_Status", "Order_Status", "Order_Status_ID", GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID"))) & vbCrLf
'        End If
'        rst.MoveNext
'    Loop
'End If
'
'rst.Close
'
'SQL = "SELECT * FROM BBCMG_Preview_Tracker WHERE DatePreviewReady IS NULL and Targetdate > '" & Format(DateAdd("h", 24, NowDate), "YYYY-MM-DD hh:nn:ss") & "' ORDER BY TargetDate;"
'rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
'
'If rst.RecordCount <> 0 Then
'
'    emailtext = emailtext & vbCrLf & "The following items are over 24 hours to go:" & vbCrLf & vbCrLf
'    rst.MoveFirst
'    Do While Not rst.EOF
'        If GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Tape Arrived") _
'        Or GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Ingest in Process") _
'        Or GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID")) = GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Preview Generation in Process") Then
'            emailtext = emailtext & "Barcode: " & rst("Barcode") & ", Arrived: " & rst("DateTapeArrived") & ", Target: " & rst("TargetDate") & ", Status: " & GetData("BBCMG_Order_Status", "Order_Status", "Order_Status_ID", GetData("BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", rst("Preview_Order_ID"))) & vbCrLf
'        End If
'        rst.MoveNext
'    Loop
'End If
'
'rst.Close
'
'Debug.Print emailtext
'
'If emailtext <> "" Then
'
'    SQL = "SELECT * FROM trackernotification WHERE contractgroup = 'BBC' AND trackermessageID = 48;"
'    rst.Open SQL, cnn, 3, 3
'    If rst.RecordCount > 0 Then
'        rst.MoveFirst
'        Do While Not rst.EOF
'            SendSMTPMail rst("email"), rst("fullname"), "BBCMG Preview Encoding work that can (and should) be done", emailtext, "", True, "", ""
'            rst.MoveNext
'        Loop
'    End If
'    rst.Close
'    SQL = "INSERT INTO Dashboard_Messages (Contact_Group, Tracker_Message_ID, Message_Title, Message_Body) VALUES ('BBC', 48, 'BBCMG Preview Encoding work that can (and should) be done', '" & emailtext & "');"
'    cnn.Execute SQL
'
'End If
'
'Second, run through BBC DADC
emailtext = ""
SQL = "SELECT * FROM tracker_dadc_item WHERE stagefield4 IS NULL AND stagefield13 IS NULL AND (rejected IS NULL or rejected = 0) AND companyID = 1261 AND Targetdate <= '" & FormatSQLDate(DateAdd("h", 4, NowDate)) & "' ORDER BY TargetDate;"
rst.Open SQL, cnn, adOpenStatic, adLockOptimistic

If rst.RecordCount <> 0 Then

    emailtext = emailtext & "The following items have either failed SLA or will fail if not completed in 4 hours:" & vbCrLf & vbCrLf
    rst.MoveFirst
    Do While Not rst.EOF
        emailtext = emailtext & "Barcode: " & rst("NewBarcode") & ", Arrived: " & rst("stagefield1") & ", Target: " & rst("TargetDate") & vbCrLf
        rst.MoveNext
    Loop
End If

rst.Close

SQL = "SELECT * FROM tracker_dadc_item WHERE stagefield4 IS NULL AND stagefield13 IS NULL AND (rejected IS NULL or rejected = 0) AND companyID = 1261 AND Targetdate <= '" & FormatSQLDate(DateAdd("h", 8, NowDate)) & "' AND Targetdate > '" & FormatSQLDate(DateAdd("h", 4, NowDate)) & "' ORDER BY TargetDate;"
rst.Open SQL, cnn, adOpenStatic, adLockOptimistic

If rst.RecordCount <> 0 Then

    emailtext = emailtext & vbCrLf & "The following items are between 8 hours and 4 hours to go:" & vbCrLf & vbCrLf
    rst.MoveFirst
    Do While Not rst.EOF
        emailtext = emailtext & "Barcode: " & rst("NewBarcode") & ", Arrived: " & rst("stagefield1") & ", Target: " & rst("TargetDate") & vbCrLf
        rst.MoveNext
    Loop
End If

rst.Close

SQL = "SELECT * FROM tracker_dadc_item WHERE stagefield4 IS NULL AND stagefield13 IS NULL AND (rejected IS NULL or rejected = 0) AND companyID = 1261 AND Targetdate <= '" & FormatSQLDate(DateAdd("h", 24, NowDate)) & "' AND Targetdate > '" & FormatSQLDate(DateAdd("h", 8, NowDate)) & "' ORDER BY TargetDate;"
rst.Open SQL, cnn, adOpenStatic, adLockOptimistic

If rst.RecordCount <> 0 Then

    emailtext = emailtext & vbCrLf & "The following items are between 24 hours and 8 hours to go:" & vbCrLf & vbCrLf
    rst.MoveFirst
    Do While Not rst.EOF
        emailtext = emailtext & "Barcode: " & rst("NewBarcode") & ", Arrived: " & rst("stagefield1") & ", Target: " & rst("TargetDate") & vbCrLf
        rst.MoveNext
    Loop
End If

rst.Close

SQL = "SELECT * FROM tracker_dadc_item WHERE stagefield4 IS NULL AND stagefield13 IS NULL AND (rejected IS NULL or rejected = 0) AND companyID = 1261 AND Targetdate > '" & FormatSQLDate(DateAdd("h", 24, NowDate)) & "' ORDER BY TargetDate;"
rst.Open SQL, cnn, adOpenStatic, adLockOptimistic

If rst.RecordCount <> 0 Then

    emailtext = emailtext & vbCrLf & "The following items are over 24 hours to go:" & vbCrLf & vbCrLf
    rst.MoveFirst
    Do While Not rst.EOF
        emailtext = emailtext & "Barcode: " & rst("NewBarcode") & ", Arrived: " & rst("stagefield1") & ", Target: " & rst("TargetDate") & vbCrLf
        rst.MoveNext
    Loop
End If

rst.Close

Debug.Print emailtext

If emailtext <> "" Then

    SQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 49;"
    rst.Open SQL, cnn, 3, 3
    If rst.RecordCount > 0 Then
        rst.MoveFirst
        Do While Not rst.EOF
            SendSMTPMail rst("email"), rst("fullname"), "DADC BBC Tracker work that can (and should) be done", emailtext, "", True, "", ""
            rst.MoveNext
        Loop
    End If
    rst.Close
    SQL = "INSERT INTO Dashboard_Messages (Contact_Group, Tracker_Message_ID, Message_Title, Message_Body) VALUES ('BBC', 49, 'BBCMG Preview Encoding work that can (and should) be done', '" & emailtext & "');"
    cnn.Execute SQL

End If

''Third, run through BBCWW
'emailtext = ""
'SQL = "SELECT * FROM vw_Tracker_BBCWW WHERE completeddate IS NULL AND cancelleddate IS NULL AND (rejecteddate IS NULL) AND companyID = 570 AND Targetdate <= '" & FormatSQLDate(DateAdd("h", 4, NowDate)) & "' ORDER BY TargetDate;"
'rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
'
'If rst.RecordCount <> 0 Then
'
'    emailtext = emailtext & "The following items have either failed SLA or will fail if not completed in 4 hours:" & vbCrLf & vbCrLf
'    rst.MoveFirst
'    Do While Not rst.EOF
'        emailtext = emailtext & "Barcode: " & rst("MasterBarcode") & ", JobID: " & rst("jobID") & ", Arrived: " & rst("DateMasterArrived") & ", Due Date: " & rst("deadlinedate") & ", Target: " & rst("TargetDate") & vbCrLf
'        rst.MoveNext
'    Loop
'End If
'
'rst.Close
'
'SQL = "SELECT * FROM vw_Tracker_BBCWW WHERE completeddate IS NULL AND cancelleddate IS NULL AND (rejecteddate IS NULL) AND companyID = 570 AND Targetdate <= '" & FormatSQLDate(DateAdd("h", 8, NowDate)) & "' AND Targetdate > '" & FormatSQLDate(DateAdd("h", 4, NowDate)) & "' ORDER BY TargetDate;"
'rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
'
'If rst.RecordCount <> 0 Then
'
'    emailtext = emailtext & vbCrLf & "The following items are between 8 hours and 4 hours to go:" & vbCrLf & vbCrLf
'    rst.MoveFirst
'    Do While Not rst.EOF
'        emailtext = emailtext & "Barcode: " & rst("MasterBarcode") & ", JobID: " & rst("jobID") & ", Arrived: " & rst("DateMasterArrived") & ", Due Date: " & rst("deadlinedate") & ", Target: " & rst("TargetDate") & vbCrLf
'        rst.MoveNext
'    Loop
'End If
'
'rst.Close
'
'SQL = "SELECT * FROM vw_Tracker_BBCWW WHERE completeddate IS NULL AND cancelleddate IS NULL AND (rejecteddate IS NULL) AND companyID = 570 AND Targetdate <= '" & FormatSQLDate(DateAdd("h", 24, NowDate)) & "' AND Targetdate > '" & FormatSQLDate(DateAdd("h", 8, NowDate)) & "' ORDER BY TargetDate;"
'rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
'
'If rst.RecordCount <> 0 Then
'
'    emailtext = emailtext & vbCrLf & "The following items are between 24 hours and 8 hours to go:" & vbCrLf & vbCrLf
'    rst.MoveFirst
'    Do While Not rst.EOF
'        emailtext = emailtext & "Barcode: " & rst("MasterBarcode") & ", JobID: " & rst("jobID") & ", Arrived: " & rst("DateMasterArrived") & ", Due Date: " & rst("deadlinedate") & ", Target: " & rst("TargetDate") & vbCrLf
'        rst.MoveNext
'    Loop
'End If
'
'rst.Close
'
'SQL = "SELECT * FROM vw_Tracker_BBCWW WHERE completeddate IS NULL AND cancelleddate IS NULL AND (rejecteddate IS NULL) AND companyID = 570 AND Targetdate > '" & FormatSQLDate(DateAdd("h", 24, NowDate)) & "' ORDER BY TargetDate;"
'rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
'
'If rst.RecordCount <> 0 Then
'
'    emailtext = emailtext & vbCrLf & "The following items are over 24 hours to go:" & vbCrLf & vbCrLf
'    rst.MoveFirst
'    Do While Not rst.EOF
'        emailtext = emailtext & "Barcode: " & rst("MasterBarcode") & ", JobID: " & rst("jobID") & ", Arrived: " & rst("DateMasterArrived") & ", Due Date: " & rst("deadlinedate") & ", Target: " & rst("TargetDate") & vbCrLf
'        rst.MoveNext
'    Loop
'End If
'
'rst.Close
'
'Debug.Print emailtext
'
'If emailtext <> "" Then
'
'    SQL = "SELECT * FROM trackernotification WHERE contractgroup = 'BBC' AND trackermessageID = 50;"
'    rst.Open SQL, cnn, 3, 3
'    If rst.RecordCount > 0 Then
'        rst.MoveFirst
'        Do While Not rst.EOF
'            SendSMTPMail rst("email"), rst("fullname"), "BBCWW Int Ops Tape->Tape Job items that can (and should) be done", emailtext, "", True, "", ""
'            rst.MoveNext
'        Loop
'    End If
'    rst.Close
'    SQL = "INSERT INTO Dashboard_Messages (Contact_Group, Tracker_Message_ID, Message_Title, Message_Body) VALUES ('BBC', 50, 'BBCWW Int Ops Tape->Tape Job items that can (and should) be done', '" & emailtext & "');"
'    cnn.Execute SQL
'
'End If

''Fourth, run through BBCWW, looking for tapes that have arrived that cannot meet the deadline un der the SLA
'emailtext = ""
'SQL = "SELECT * FROM vw_Tracker_BBCWW WHERE completeddate IS NULL AND cancelleddate IS NULL AND rejecteddate IS NULL AND companyID = 570 AND MasterBarcode IS NOT NULL and MasterBarcode <> '' AND Targetdate IS NOT NULL AND CAST(Targetdate AS DATE) > cast(deadlinedate AS DATE) ORDER BY TargetDate;"
'rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
'
'If rst.RecordCount <> 0 Then
'
'    emailtext = emailtext & "The following items are now at RRmedia, but cannot be completed by the original Deadline Date:" & vbCrLf & vbCrLf
'    rst.MoveFirst
'    Do While Not rst.EOF
'        emailtext = emailtext & "Barcode: " & rst("MasterBarcode") & ", JobID: " & rst("jobID") & ", Arrived: " & Format(rst("DateMasterArrived"), "DD/MM/YYYY") & ", Target: " & Format(rst("TargetDate"), "DD/MM/YYYY") & ", Original Deadline: " & Format(rst("deadlinedate"), "DD/MM/YYYY") & vbCrLf
'        rst.MoveNext
'    Loop
'End If
'
'rst.Close
'
'Debug.Print emailtext
'
'If emailtext <> "" Then
'
'    SQL = "SELECT * FROM trackernotification WHERE contractgroup = 'BBC' AND trackermessageID = 50;"
'    rst.Open SQL, cnn, 3, 3
'    If rst.RecordCount > 0 Then
'        rst.MoveFirst
'        Do While Not rst.EOF
'            SendSMTPMail rst("email"), rst("fullname"), "BBCWW Int Ops Tape->Tape Job(s) that cannot meet their Deadline Date", emailtext, "", True, "", ""
'            rst.MoveNext
'        Loop
'    End If
'    rst.Close
'    SQL = "INSERT INTO Dashboard_Messages (Contact_Group, Tracker_Message_ID, Message_Title, Message_Body) VALUES ('BBC', 50, 'BBCWW Int Ops Tape->Tape Job(s) that cannot meet their Deadline Date', '" & emailtext & "');"
'    cnn.Execute SQL
'
'End If

cnn.Close

Set rst = Nothing
Set cnn = Nothing

End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset

    l_rstGetData.Open l_strSQL, cnn, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing


    '<EhFooter>
    Exit Function

GetData_Err:
    MsgBox Err.Description & vbCrLf & _
           "in prjJCAFileManager.modTest.GetData " & _
           "at line " & Erl, _
           vbExclamation + vbOKOnly, "Application Error"
    Resume Next
    '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function



