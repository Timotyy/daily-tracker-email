Attribute VB_Name = "DailyTrackerModule"
Option Explicit

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "MX1 London Operations Department"
Const g_strFullUSerName = "MX1 London Operations"

Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Public g_strConnection As String

Sub Main()

Dim cnn As ADODB.Connection, rst As ADODB.Recordset, rstclip As ADODB.Recordset, rstHoliday As ADODB.Recordset
Dim SQL As String, DaysPast As Integer, l_strCommandLine As String
Dim emailtext As String, ClipID As Long, TempDate As String, Channel As String
Dim EmailTo As String, EmailName As String
Dim CountDown As Long

Set cnn = New ADODB.Connection

Dim CommandString As String
CommandString = Command()

g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bssql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

'g_strConnection = "DRIVER=SQL Server;SERVER=jca-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=MX1TEST-sql-3;Failover_Partner=MX1TEST-sql-4;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

cnn.Open g_strConnection

Set rst = New ADODB.Recordset
'Set rstclip = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strUserEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strUserEmailName = rst("value")
End If
rst.Close

'Check whether this is a working day or not....
SQL = "SELECT DayDescription FROM Holiday_Dates WHERE Fulldate = '" & Format(Now, "mm/dd/yy") & "';"
Set rstHoliday = New ADODB.Recordset
        
rstHoliday.Open SQL, cnn, 3, 3
If rstHoliday.RecordCount = 1 Then
    If rstHoliday("DayDescription") = "Working Day" Then

        'Checks iTunes Tracker for all items that are completable but not completed and counts down the Target Countdown for them.
        emailtext = ""
        l_strCommandLine = Command()
        'DaysPast = Val(l_strCommandLine)
        'TempDate = Day(DateAdd("d", -DaysPast, Now)) & " " & Left(MonthName(Month(DateAdd("d", -DaysPast, Now))), 3) & " " & Right(Year(DateAdd("d", -DaysPast, Now)), 2)
        
        SQL = "SELECT * FROM tracker_iTunes_item WHERE readytobill = 0 and targetdate IS NOT NULL;"
        rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
        
        If rst.RecordCount <> 0 Then
        
            rst.MoveFirst
            Do While Not rst.EOF
                CountDown = rst("TargetDateCountdown")
                If CountDown > 0 Then CountDown = CountDown - 1
                rst("TargetDateCountdown") = CountDown
                rst.Update
                rst.MoveNext
            Loop
        End If
        
        rst.Close
        
        'Checks BBC DADC Tracker for all items that are completable but not completed and counts down the Target Countdown for them.
        emailtext = ""
        l_strCommandLine = Command()
        'DaysPast = Val(l_strCommandLine)
        'TempDate = Day(DateAdd("d", -DaysPast, Now)) & " " & Left(MonthName(Month(DateAdd("d", -DaysPast, Now))), 3) & " " & Right(Year(DateAdd("d", -DaysPast, Now)), 2)
                
        SQL = "SELECT * FROM tracker_DADC_item WHERE readytobill = 0 and targetdate IS NOT NULL;"
        rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
        
        If rst.RecordCount <> 0 Then
        
            rst.MoveFirst
            Do While Not rst.EOF
                CountDown = rst("TargetDateCountdown")
                If CountDown > 0 Then CountDown = CountDown - 1
                rst("TargetDateCountdown") = CountDown
                rst.Update
                rst.MoveNext
            Loop
        End If
        
        rst.Close
        
        'Checks Turner Tracker for all items that are completable but not completed and counts down the Target Countdown for them.
        emailtext = ""
        l_strCommandLine = Command()
        'DaysPast = Val(l_strCommandLine)
        'TempDate = Day(DateAdd("d", -DaysPast, Now)) & " " & Left(MonthName(Month(DateAdd("d", -DaysPast, Now))), 3) & " " & Right(Year(DateAdd("d", -DaysPast, Now)), 2)
                
        SQL = "SELECT * FROM tracker_turner_item WHERE readytobill = 0 and TargetDate IS NOT NULL;"
        rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
        
        If rst.RecordCount <> 0 Then
        
            rst.MoveFirst
            Do While Not rst.EOF
                CountDown = rst("TargetDateCountdown")
                If CountDown > 0 Then CountDown = CountDown - 1
                rst("TargetDateCountdown") = CountDown
                rst.Update
                rst.MoveNext
            Loop
        End If
        
        rst.Close
        
        'Checks Svensk Ingest Tracker for all items that are completable but not completed and counts down the Target Countdown for them.
        emailtext = ""
        l_strCommandLine = Command()
        'DaysPast = Val(l_strCommandLine)
        'TempDate = Day(DateAdd("d", -DaysPast, Now)) & " " & Left(MonthName(Month(DateAdd("d", -DaysPast, Now))), 3) & " " & Right(Year(DateAdd("d", -DaysPast, Now)), 2)
        
        SQL = "SELECT * FROM tracker_svensk_item WHERE readytobill = 0 and targetdate IS NOT NULL;"
        rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
        
        If rst.RecordCount <> 0 Then
        
            rst.MoveFirst
            Do While Not rst.EOF
                CountDown = rst("TargetDateCountdown")
                If CountDown > 0 Then CountDown = CountDown - 1
                rst("TargetDateCountdown") = CountDown
                rst.Update
                rst.MoveNext
            Loop
        End If
        
        rst.Close
        
        'Checks BBCWW Orders Tracker for all items that are completable but not completed and counts down the Target Countdown for them.
        emailtext = ""
        l_strCommandLine = Command()
        'DaysPast = Val(l_strCommandLine)
        'TempDate = Day(DateAdd("d", -DaysPast, Now)) & " " & Left(MonthName(Month(DateAdd("d", -DaysPast, Now))), 3) & " " & Right(Year(DateAdd("d", -DaysPast, Now)), 2)
        
        SQL = "SELECT * FROM jobdetail WHERE completeddate IS NULL and targetdate IS NOT NULL;"
        rst.Open SQL, cnn, adOpenStatic, adLockOptimistic
        
        If rst.RecordCount <> 0 Then
        
            rst.MoveFirst
            Do While Not rst.EOF
                CountDown = rst("TargetDateCountdown")
                If CountDown > 0 Then CountDown = CountDown - 1
                rst("TargetDateCountdown") = CountDown
                rst.Update
                rst.MoveNext
            Loop
        End If
        
        rst.Close
    End If
    
End If

rstHoliday.Close
Set rstHoliday = Nothing

cnn.Close

'Set rstclip = Nothing
Set rst = Nothing
Set cnn = Nothing

End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
100 l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

102 l_con.Open "dsn=cetasql"
104 l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

106 If Not l_rstGetData.EOF Then
    
108     If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
110         GetData = l_rstGetData(lp_strFieldToReturn)
112         GoTo Proc_CloseDB
        End If

    Else
114     GoTo Proc_CloseDB

    End If

116 Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
118     GetData = ""
120 Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
122     GetData = 0
124 Case adDate, adDBDate, adDBTime, adDBTimeStamp
126     GetData = 0
128 Case Else
130     GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

132 l_rstGetData.Close
134 Set l_rstGetData = Nothing

136 l_con.Close
138 Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function



